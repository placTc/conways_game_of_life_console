#include "Grid.h"

Grid::Grid()
{	
	for (int i = 0; i < GRID_SIZE; i++)
	{
		for (int j = 0; j < GRID_SIZE; j++)
		{
			this->grid[i][j] = 0;
		}
	}
}

Grid::~Grid()
{
}

void Grid::outputGrid()
{
	for (int i = 0; i < GRID_SIZE; i++)
	{
		for (int j = 0; j < GRID_SIZE; j++)
		{
			if (grid[i][j])
				std::cout << "X";
			else
				std::cout << " ";
		}
		std::cout << std::endl;
	}
}

void Grid::changeCells()
{
	for (int i = 0; i < GRID_SIZE; i++)
	{
		for (int j = 0; j < GRID_SIZE; j++)
		{
			checkCell(i, j);
		}
	}
}

void Grid::checkCell(int y, int x)
{
	int surroundCounter = 0;

	if (x > MIN_GRID_VAL && y > MIN_GRID_VAL && x < MAX_GRID_VAL && y < MAX_GRID_VAL)
	{
		for (int i = -1; i <= 1; i++)
		{
			for (int j = -1; j <= 1; j++)
			{
				if (checkLife(grid[(x + i)][(y + j)]))
					surroundCounter++;
			}
		}
	}

	if (grid[x][y] == 1)
	{
		surroundCounter--;
		if (surroundCounter < 2)
		{
			grid[x][y] = CELL_TD;
		}
		if (surroundCounter > 3)
		{
			grid[x][y] = CELL_TD;
		}
	}
	else
	{
		if (surroundCounter == 3)
		{
			grid[x][y] = CELL_TL;
		}
	}
}

bool Grid::checkLife(int cell)
{
	return (cell == CELL_L || cell == CELL_TD);
}

void Grid::proceed()
{
	for (int i = 0; i < GRID_SIZE; i++)
	{
		for (int j = 0; j < GRID_SIZE; j++)
		{
			if (grid[i][j] == CELL_TL)
			{
				grid[i][j] = CELL_L;
			}
			if (grid[i][j] == CELL_TD)
			{
				grid[i][j] = CELL_D;
			}
		}
	}
}

void Grid::setCellAlive(int x, int y)
{
	grid[y][x] = 1;
}
