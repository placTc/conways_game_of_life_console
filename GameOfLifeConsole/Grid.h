#pragma once
#define GRID_SIZE 29
#define MIN_GRID_VAL 0
#define MAX_GRID_VAL (GRID_SIZE - 1)
#define CELL_D 0
#define CELL_L 1
#define CELL_TD 2
#define CELL_TL 3

#include <iostream>

class Grid
{
private:
	int grid[GRID_SIZE][GRID_SIZE];
	void checkCell(int x, int y);
	bool checkLife(int cell);
public:
	Grid();
	~Grid();
	void outputGrid();
	void changeCells();
	void proceed();
	void setCellAlive(int x, int y);
};

