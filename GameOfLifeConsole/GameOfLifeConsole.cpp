﻿#include <iostream>
#include <thread>
#include <chrono>
#include "Grid.h"

int main()
{
	const std::chrono::milliseconds sleepTime(1);
	Grid grid;

	grid.setCellAlive(13, 13);
	grid.setCellAlive(14, 13);
	grid.setCellAlive(14, 11);
	grid.setCellAlive(16, 12);
	grid.setCellAlive(17, 13);
	grid.setCellAlive(18, 13);
	grid.setCellAlive(19, 13);

	grid.outputGrid();
	std::this_thread::sleep_for(sleepTime);
	system("CLS");

	while (true)
	{
		grid.changeCells();
		grid.proceed();
		grid.outputGrid();
		std::this_thread::sleep_for(sleepTime);
		system("CLS");
	}
}
